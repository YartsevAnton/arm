#include <stm32f10x.h>
#include <stm32f10x_conf.h>

#include <stm32f10x_rcc.h>
#include <stm32f10x_gpio.h>
#define RCC_GPIO RCC_APB2Periph_GPIOA
#define LED_PORT GPIOA
#define LED1_PIN GPIO_Pin_1 //PA1 (+jumper) - green led
//#define LED2_PIN GPIO_Pin_2

//--------------------------delay------------------------------------
volatile uint32_t msTicks;

void SysTick_Handler (void) //Enter here every 1 ms
{
  msTicks++;
}
void Delay(uint32_t dlyTicks)
{
  uint32_t curTicks;

  curTicks = msTicks;
  while ((msTicks - curTicks) < dlyTicks);
}
//--------------------------------------------------------------------

int main(void) {
	/* SystemInit() ��� ���������� � startup_stm32f10x_md_vl.S */
    SysTick_Config(SystemCoreClock/1000); // 'prescaler' of SysTick for 1 ms (for delay func)

	GPIO_InitTypeDef GPIO_InitStructure;

	RCC_APB2PeriphClockCmd(RCC_GPIO, ENABLE);

	GPIO_InitStructure.GPIO_Pin = LED1_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;

	GPIO_Init( LED_PORT , &GPIO_InitStructure);

	while (1) {
		LED_PORT->ODR ^= LED1_PIN;
		Delay(300);
	}
	return 0;
}
